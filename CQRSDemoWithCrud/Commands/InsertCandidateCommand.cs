﻿using MediatR;

namespace CQRSDemoWithCrud;

public record InsertCandidateCommand(CandidateModel Model) : IRequest<Candidate>;

