﻿namespace CQRSDemoWithCrud;

public class Seeder
{
    private readonly Context _context;

    public Seeder(Context context)
    {
        _context = context;
    }

    public void Seed()
    {
        var candidates = new List<Candidate>
        {
            new Candidate
            {
                Name = "John",
                CurrentCompany = "ABC",
                NoticePeriod = "2 months",
                PrimarySkills = "C#",
                SecondarySkills = "SQL",
                Location = "Bangalore",
                ContactNumber = "1234567890",
                Email = "",
                Designation = "Software Engineer",
                Experience = "2 years",
                CTC = "5 LPA"
            },
            new Candidate
            {
                Name = "Jane",
                CurrentCompany = "XYZ",
                NoticePeriod = "1 month",
                PrimarySkills = "Java",
                SecondarySkills = "SQL",
                Location = "Bangalore",
                ContactNumber = "1234567890",
                Email = "",
                Designation = "Software Engineer",
                Experience = "2 years",
                CTC = "5 LPA"
            },
            new Candidate
            {
                Name = "Jack",
                CurrentCompany = "PQR",
                NoticePeriod = "2 months",
                PrimarySkills = "C#",
                SecondarySkills = "SQL",
                Location = "Bangalore",
                ContactNumber = "1234567890",
                Email = "",
                Designation = "Software Engineer",
                Experience = "2 years",
                CTC = "5 LPA"
            },
            new Candidate
            {
                Name = "Jill",
                CurrentCompany = "LMN",
                NoticePeriod = "1 month",
                PrimarySkills = "Java",
                SecondarySkills = "SQL",
                Location = "Bangalore",
                ContactNumber = "1234567890",
                Email = "",
                Designation = "Software Engineer",
                Experience = "2 years",
                CTC = "5 LPA"
            }
        };
        _context.Candidates.AddRange(candidates);
        _context.SaveChanges();
    }
}
