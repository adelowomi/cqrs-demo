﻿using Microsoft.EntityFrameworkCore;

namespace CQRSDemoWithCrud;

public class Context : DbContext
{
    public Context(DbContextOptions<Context> options) : base(options)
    {
    }

    // protected override void OnConfiguring(DbContextOptionsBuilder options)
    // {
    //     options.UseInMemoryDatabase(databaseName: "CandidateDB");
    // }
    public DbSet<Candidate> Candidates { get; set; }
}
