﻿namespace CQRSDemoWithCrud;

public class Candidate
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string CurrentCompany { get; set; }
    public string NoticePeriod { get; set; }
    public string PrimarySkills { get; set; }
    public string SecondarySkills { get; set; }
    public string Location { get; set; }
    public string ContactNumber { get; set; }
    public string Email { get; set; }
    public string Designation { get; set; }
    public string Experience { get; set; }
    public string CTC { get; set; }
}
