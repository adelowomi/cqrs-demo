﻿namespace CQRSDemoWithCrud;

public interface ICandidateService
{
    Task<IEnumerable<Candidate>> GetCandidates();
    Task<Candidate> GetCandidate(int id);
    Task<Candidate> InsertCandidate(CandidateModel model);
}
