﻿using MediatR;

namespace CQRSDemoWithCrud;

public class CandidateService : ICandidateService
{
    private readonly IMediator _mediator;

    public CandidateService(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<IEnumerable<Candidate>> GetCandidates()
    {
        var candidates = await _mediator.Send(new ListCandidateQuery());
        return candidates;
    }

    public async Task<Candidate> GetCandidate(int id)
    {
        var candidate = await _mediator.Send(new GetCandidateQuery(id));
        return candidate;
    }

    public async Task<Candidate> InsertCandidate(CandidateModel model)
    {
        var candidate = await _mediator.Send(new InsertCandidateCommand(model));
        return candidate;
    }
}
