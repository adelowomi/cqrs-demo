﻿namespace CQRSDemoWithCrud;

public record class CandidateModel(int Id, string Name, string Email, string Mobile, string Skills, string Experience, string CurrentCTC, string PrimarySkills, string CurrentCompany, string Designation, string Location, string SecondarySkills, string NoticePeriod, string CreatedBy, string UpdatedBy);
