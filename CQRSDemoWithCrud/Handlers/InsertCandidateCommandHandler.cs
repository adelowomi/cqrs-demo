﻿using MediatR;

namespace CQRSDemoWithCrud;

public class InsertCandidateCommandHandler : IRequestHandler<InsertCandidateCommand, Candidate>
{
    private readonly Context _context;

    public InsertCandidateCommandHandler(Context context)
    {
        _context = context;
    }

    public Task<Candidate> Handle(InsertCandidateCommand request, CancellationToken cancellationToken)
    {
        var candidate = new Candidate
        {
            Name = request.Model.Name,
            Email = request.Model.Email,
            Experience = request.Model.Experience,
            NoticePeriod = request.Model.NoticePeriod,
            CTC = request.Model.CurrentCTC,
            ContactNumber = request.Model.Mobile,
            Designation = request.Model.Designation,
            Location = request.Model.Location,
            PrimarySkills = request.Model.PrimarySkills,
            SecondarySkills = request.Model.SecondarySkills,
            CurrentCompany = request.Model.CurrentCompany,
        };

        _context.Candidates.Add(candidate);
        _context.SaveChanges();
        return Task.FromResult(candidate);
    }
}
