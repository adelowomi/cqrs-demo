﻿using MediatR;

namespace CQRSDemoWithCrud;

public class GetCandidateQueryHandler : IRequestHandler<GetCandidateQuery, Candidate>
{
    private readonly Context _context;

    public GetCandidateQueryHandler(Context context)
    {
        _context = context;
    }

    public async Task<Candidate> Handle(GetCandidateQuery request, CancellationToken cancellationToken)
    {
        return await _context.Candidates.FindAsync(new object?[] { request.Id }, cancellationToken: cancellationToken);
    }
}