﻿using MediatR;

namespace CQRSDemoWithCrud;

public class ListCandidateQueryHandler : IRequestHandler<ListCandidateQuery, IEnumerable<Candidate>>
{
    private readonly Context _context;

    public ListCandidateQueryHandler(Context context)
    {
        _context = context;
    }

    public Task<IEnumerable<Candidate>> Handle(ListCandidateQuery request, CancellationToken cancellationToken)
    {
        var candidates = _context.Candidates.AsEnumerable();
        return Task.FromResult(candidates);
    }
}
