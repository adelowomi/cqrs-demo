﻿using Microsoft.AspNetCore.Mvc;

namespace CQRSDemoWithCrud;


[ApiController]
[Route("api/[controller]")]


public class CandidateController : ControllerBase
{
    private readonly ICandidateService _candidateService;

    public CandidateController(ICandidateService candidateService)
    {
        _candidateService = candidateService;
    }

    [HttpGet("list", Name = nameof(ListCandidates))]
    [ProducesResponseType(200)]
    public async Task<IActionResult> ListCandidates()
    {
        var candidates = await _candidateService.GetCandidates();
        return Ok(candidates);
    }

    [HttpGet("{id}", Name = nameof(GetCandidate))]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    public async Task<IActionResult> GetCandidate(int id)
    {
        var candidate = await _candidateService.GetCandidate(id);
        if (candidate == null)
        {
            return NotFound();
        }
        return Ok(candidate);
    }

    [HttpPost("create", Name = nameof(CreateCandidate))]
    [ProducesResponseType(201)]
    [ProducesResponseType(400)]
    public async Task<IActionResult> CreateCandidate(CandidateModel model)
    {
        var candidate = await _candidateService.InsertCandidate(model);
        return CreatedAtRoute(nameof(GetCandidate), new { id = candidate.Id }, candidate);
    }
}
