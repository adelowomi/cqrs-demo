﻿using MediatR;
namespace CQRSDemoWithCrud;


public record ListCandidateQuery : IRequest<IEnumerable<Candidate>>;
