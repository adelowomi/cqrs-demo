﻿using MediatR;

namespace CQRSDemoWithCrud;

public record GetCandidateQuery(int Id) : IRequest<Candidate>;
